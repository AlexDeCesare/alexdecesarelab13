package edu.westga.cs1302.scenebuilder.model;

/**
 * This class does all of the math given two input numbers
 * 
 * @author Alex DeCesare
 * @version 14-July-2020
 */

public class NumberMath {
	
	private int firstNumber;
	private int secondNumber;
	
	/**
	 * The constructor for the Math class
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param firstNumber the firstNumber to perform math on
	 * @param secondNumber the secondNumber to perform math on
	 */
	
	public NumberMath(int firstNumber, int secondNumber) {
		
		this.firstNumber = firstNumber;
		this.secondNumber = secondNumber;
		
	}
	
	/**
	 * The getter for the first number
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the first number
	 */
	public int getFirstNumber() {
		return this.firstNumber;
	}
	
	/**
	 * The getter for the second number
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the second number
	 */
	public int getSecondNumber() {
		return this.secondNumber;
	}
	
	/**
	 * Adds the first and second number together
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the two numbers added together
	 */
	public int addNumbers() {
		int addedNumbers = this.getFirstNumber() + this.getSecondNumber();
		
		return addedNumbers;
	}
	
	/**
	 * The toString method for the Math class
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the string representation of the math class
	 */
	
	@Override
	public String toString() {
		return "The First Number For Math: " + this.getFirstNumber() + " The Second Number For Math: " + this.getSecondNumber();
	}

}
