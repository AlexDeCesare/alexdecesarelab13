package edu.westga.cs1302.scenebuilder.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;

/**
 * Connects the view model and the gui
 * 
 * @author Alex DeCesare
 * @version 14-July-2020
 */

public class SceneBuilderCodeBehind {
	
	private static final String THE_NUMBER_FORMAT_ALERT_TEXT = "Please only input numbers into the input boxes";
	private ViewModel theViewModel;

    @FXML
    private Label lblFirstNumber;

    @FXML
    private Label lblSecondNumber;

    @FXML
    private TextArea txtInputFirstNumber;

    @FXML
    private TextArea txtInputSecondNumber;

    @FXML
    private Button btnCalculateSum;

    @FXML
    private Button btnExit;

    @FXML
    private Label lblOutput;
    
	/**
	* The constructor for the code behind class
	* 
	* @precondition none
	* @postcondition viewModel = getViewModel
	*/
    
	public SceneBuilderCodeBehind() {
		this.theViewModel = new ViewModel();
	}

    @FXML
    void exit(ActionEvent event) {

    	System.exit(0);
    	
    }

    @FXML
    void setOutput(ActionEvent event) { 	
    	try {
    		
        	this.theViewModel.setFirstNumberInput(this.txtInputFirstNumber.getText());
        	this.theViewModel.setSecondNumberInput(this.txtInputSecondNumber.getText());
        	
        	this.theViewModel.setOutput();
        	this.lblOutput.setText(this.theViewModel.getOutput());
    		
    	} catch (NumberFormatException theNumberFormatException) {
    		
    		Alert theAlert = new Alert(AlertType.ERROR); 
    		theAlert.setContentText(THE_NUMBER_FORMAT_ALERT_TEXT);
    		theAlert.showAndWait();
    		
    	} catch (IllegalArgumentException theIllegalArgumentException) {
    		
    		Alert theAlert = new Alert(AlertType.ERROR); 
    		theAlert.setContentText(theAlert.getContentText());
    		theAlert.showAndWait();
    	}
    }

}
