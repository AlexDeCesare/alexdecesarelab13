package edu.westga.cs1302.scenebuilder.view;

import edu.westga.cs1302.scenebuilder.model.NumberMath;

/**
 * This is the view model class that stands between the model and the view
 * 
 * @author Alex DeCesare
 * @version 14-July-2020
 */

public class ViewModel {
	
	private NumberMath theMath;	
	private String theOutput;
	private int theFirstNumberInput;
	private int theSecondNumberInput;
	
	/**
	 * The constructor for the view model 
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	
	public ViewModel() {

	}
	
	/**
	 * Gets the math class
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the math class
	 */
	
	public NumberMath getTheMath() {
		return this.theMath;
	}
	
	/**
	 * Gets the first number input
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the first number input
	 */
	
	public int getFirstNumberInput() {
		return this.theFirstNumberInput;
	}
	
	/**
	 * Gets the first number input
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the first number input
	 */
	
	public int getSecondNumberInput() {
		return this.theSecondNumberInput;
	}
	
	/**
	 * Sets the first number input
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param theFirstNumberInput the first number input
	 */
	
	public void setFirstNumberInput(String theFirstNumberInput) {
		int parsedInput = this.parseIntegers(theFirstNumberInput);
		this.theFirstNumberInput = parsedInput;
	}
	
	/**
	 * Gets the output
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the output
	 */

	public String getOutput() {
		return this.theOutput;
	}
	
	/**
	 * Sets the second number input
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param theSecondNumberInput the second number input
	 */
	
	public void setSecondNumberInput(String theSecondNumberInput) {
		int parsedInput = this.parseIntegers(theSecondNumberInput);
		this.theSecondNumberInput = parsedInput;
	}
	
	/**
	 * Sets the output of the numbers added together
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	
	public void setOutput() {
		this.theMath = new NumberMath(this.getFirstNumberInput(), this.getSecondNumberInput());
		this.getTheMath().addNumbers();
		this.theOutput = "The solution of the two numbers is: " + this.getTheMath().addNumbers();
	}
	
	private int parseIntegers(String theStringToParse) {
		
		int parsedInt = 0;
			
		parsedInt = Integer.parseInt(theStringToParse);
			
		return parsedInt;
		
	}

}
