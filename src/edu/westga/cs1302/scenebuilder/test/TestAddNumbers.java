package edu.westga.cs1302.scenebuilder.test;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.Test;

import edu.westga.cs1302.scenebuilder.model.NumberMath;

class TestAddNumbers {

	@Test
	public void shouldAddNumbersWellBelowZero() {
		NumberMath testMath = new NumberMath(-500, -500);
		
		assertEquals(-1000, testMath.addNumbers());
	}
	
	@Test
	public void shouldAddNumbersOneBelowZero() {
		NumberMath testMath = new NumberMath(-1, -1);
		
		assertEquals(-2, testMath.addNumbers());
	}
	
	@Test
	public void shouldAddNumbersAtZero() {
		NumberMath testMath = new NumberMath(0, 0);
		
		assertEquals(0, testMath.addNumbers());
	}
	
	@Test
	public void shouldAddNumbersOneAboveZero() {
		NumberMath testMath = new NumberMath(1, 1);
		
		assertEquals(2, testMath.addNumbers());
	}
	
	@Test
	public void shouldAddNumbersWellAboveZero() {
		NumberMath testMath = new NumberMath(500, 500);
		
		assertEquals(1000, testMath.addNumbers());
	}
}
